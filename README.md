# numberquick

A game to play with friend

*Limitation: Game is using udp, so it can't detect if server already existed or not. Host player need to start first or others will be block forever.*

## Rule to play
All players try to find number from 1 to 100 in order as fast as they can. Player who found the most correct number is winner.

## How to run
- Install Rust:  
https://www.rust-lang.org/en-US/install.html
- Build
```
cargo build --release
```
- Run
```
./target/release/numberquick
```
