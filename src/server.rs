/*
 * Copyright (C) 2018 Do Thanh Trung <dothanhtrung.16@gmail.com>
 */

use gamedata::*;

extern crate futures;

use self::futures::try_ready;
use serde_json::Value;
use std::io;
use std::net::SocketAddr;
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::net::UdpSocket;
use tokio::prelude::*;

pub struct Server {
    pub socket: UdpSocket,
    buf: Vec<u8>,
    to_send: Option<(usize, SocketAddr)>,
    gamedata: GameData,
}

impl Server {
    pub fn new(board_size: u32, server_addr: &str) -> Self {
        let addr = server_addr.parse::<SocketAddr>().unwrap();
        let socket = UdpSocket::bind(&addr).unwrap();

        Server {
            socket: socket,
            buf: vec![0; 1024],
            to_send: None,
            gamedata: GameData::new(board_size),
        }
    }
}

impl Future for Server {
    type Item = ();
    type Error = io::Error;

    fn poll(&mut self) -> Poll<(), io::Error> {
        loop {
            if let Some((size, peer)) = self.to_send {
                let request = String::from_utf8(self.buf[..size].to_vec()).unwrap();
                let request: Value = serde_json::from_str(&request).unwrap();

                let mut action = "";
                if request["action"].is_string() {
                    action = request["action"].as_str().unwrap();
                }

                let mut id = 0;
                if request["id"].is_u64() {
                    id = request["id"].as_u64().unwrap();
                }

                if action == "new" {
                    let mut name = "";
                    if request["name"].is_string() {
                        name = request["name"].as_str().unwrap();
                    }

                    // Generate new id
                    let id = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
                    let id = id.as_secs() * 1000 + id.subsec_millis() as u64;

                    // Add new player
                    let player = Player::new(id, name.to_string());
                    self.gamedata.players.push(player);

                    // Return data to client
                    let board = serde_json::to_string(&self.gamedata.board)?;
                    let response = format!("{{\"id\": {}, \"board\": {}}}", id, board);
                    let _ = try_ready!(self.socket.poll_send_to(&response.as_bytes(), &peer));
                } else if action == "click" {
                    if request["i"].is_u64() {
                        let i = request["i"].as_u64().unwrap();
                        self.gamedata.check(i as usize, id);
                        let _ = try_ready!(self.socket.poll_send_to("correct".as_bytes(), &peer));
                    }
                } else {
                    let gamedata = serde_json::to_string(&self.gamedata)?;
                    let _ = try_ready!(self.socket.poll_send_to(&gamedata.as_bytes(), &peer));
                }
                self.to_send = None;
            }

            // If we're here then `to_send` is `None`, so we take a look for the
            // next message we're going to echo back.
            self.to_send = Some(try_ready!(self.socket.poll_recv_from(&mut self.buf)));
        }
    }
}
