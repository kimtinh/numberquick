/*
 * Copyright (C) 2018 Do Thanh Trung <dothanhtrung.16@gmail.com>
 */

use gamedata::Board;
use gamedata::Player;

use ggez::event::{self, MouseButton};
use ggez::graphics::{self, DrawMode, Font, Point2, Text};
use ggez::{Context, GameResult};
use serde_json::Value;
use std::net::SocketAddr;
use tokio::net::UdpSocket;
use tokio::prelude::*;

struct PlayerDraw {
    player: Player,
    text: Text,
    pos: (f32, f32),
}
impl PlayerDraw {
    fn new(ctx: &mut Context, player: Player, font: &Font, order: usize, h: u32) -> Self {
        let text = format!("{}: {}", player.score, player.name);
        let y = (order * (font.get_height() + 5) + 2 * font.get_height()) as f32;
        let x = (h + 10) as f32;
        PlayerDraw {
            player,
            text: Text::new(ctx, &text, font).unwrap(),
            pos: (x, y),
        }
    }

    fn update(&mut self, ctx: &mut Context, player: Player, font: &Font) {
        self.player = player;
        let text = format!("{}: {}", self.player.score, self.player.name);
        self.text = Text::new(ctx, &text, font).unwrap();
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::set_color(
            ctx,
            [
                self.player.color.0,
                self.player.color.1,
                self.player.color.2,
                1.0,
            ]
                .into(),
        )?;
        graphics::draw(ctx, &self.text, Point2::new(self.pos.0, self.pos.1), 0.0)?;
        Ok(())
    }
}

struct NumberDraw {
    text: Text,
    found: bool,
    pos: (f32, f32),
    h: f32,
    color: (f32, f32, f32),
}

impl NumberDraw {
    fn new(ctx: &mut Context, number: u32, font: &Font, pos: (f32, f32), h: f32) -> Self {
        NumberDraw {
            text: Text::new(ctx, &number.to_string(), font).unwrap(),
            found: false,
            pos,
            h,
            color: (1.0, 1.0, 1.0),
        }
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::set_color(ctx, graphics::WHITE)?;
        let w_margin = (self.h - self.text.width() as f32) / 2.0;
        let h_margin = (self.h - self.text.height() as f32) / 2.0;
        graphics::rectangle(
            ctx,
            DrawMode::Line(1.0),
            [self.pos.0, self.pos.1, self.h, self.h].into(),
        )?;
        graphics::draw(
            ctx,
            &self.text,
            Point2::new(self.pos.0 + w_margin, self.pos.1 + h_margin),
            0.0,
        )?;

        graphics::set_color(ctx, [self.color.0, self.color.1, self.color.2, 1.0].into())?;
        if self.found {
            graphics::line(
                ctx,
                &[
                    Point2::new(self.pos.0, self.pos.1),
                    Point2::new(self.pos.0 + self.h, self.pos.1 + self.h),
                ],
                1.0,
            )?;
            graphics::line(
                ctx,
                &[
                    Point2::new(self.pos.0, self.pos.1 + self.h),
                    Point2::new(self.pos.0 + self.h, self.pos.1),
                ],
                1.0,
            )?;
        }

        Ok(())
    }
}

pub struct MainState {
    id: u64,
    remote_addr: SocketAddr,
    local_addr: SocketAddr,
    players: Vec<PlayerDraw>,
    board: Vec<NumberDraw>,
    board_size: u32,
    screen_size: (u32, u32),
    last_number: Text,
    font: Font,
}

impl MainState {
    pub fn new(
        ctx: &mut Context,
        server_addr: &str,
        name: String,
        screen_size: (u32, u32),
    ) -> GameResult<MainState> {
        let remote_addr: SocketAddr = server_addr.parse().unwrap();
        let local_addr: SocketAddr = if remote_addr.is_ipv4() {
            "0.0.0.0:0"
        } else {
            "[::]:0"
        }.parse()
        .unwrap();
        let font = Font::default_font().unwrap();
        let mut s = MainState {
            id: 0,
            remote_addr,
            local_addr,
            players: Vec::new(),
            board: Vec::new(),
            board_size: 0,
            screen_size,
            last_number: Text::new(ctx, "0", &font).unwrap(),
            font,
        };

        let msg = format!("{{\"action\": \"new\", \"name\": \"{}\"}}", name);
        let r = s.request(msg);
        let r: Value = serde_json::from_str(&r).unwrap();

        s.id = r["id"].as_u64().unwrap();

        let board: Board = serde_json::from_value(r["board"].clone()).unwrap();
        s.board_size = board.size;
        for i in 0..board.numbers.len() {
            let h = (screen_size.1 / s.board_size) as f32;
            let x = (i as u32 % s.board_size) as f32 * h;
            let y = (i as u32 / s.board_size) as f32 * h;
            s.board.push(NumberDraw::new(
                ctx,
                board.numbers[i].number,
                &s.font,
                (x, y),
                h,
            ));
        }

        Ok(s)
    }

    fn request(&self, msg: String) -> String {
        let socket = UdpSocket::bind(&self.local_addr).unwrap();
        const MAX_DATAGRAM_SIZE: usize = 65_507;

        let mut r = String::from("Err");
        let processing = socket
            .send_dgram(msg, &self.remote_addr)
            .and_then(|(socket, _)| socket.recv_dgram(vec![0u8; MAX_DATAGRAM_SIZE]))
            .map(|(_, data, len, _)| r = String::from_utf8(data[..len].to_vec()).unwrap())
            .wait();
        match processing {
            Ok(_) => {}
            Err(e) => eprintln!("Encountered an error: {}", e),
        }

        r
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        let msg = "{\"action\": \"update\"}".to_owned();
        let r = self.request(msg);
        let r: Value = serde_json::from_str(&r).unwrap();

        let players: Vec<Player> = serde_json::from_value(r["players"].clone()).unwrap();
        for i in 0..self.players.len() {
            if self.players[i].player != players[i] {
                self.players[i].update(ctx, players[i].clone(), &self.font);
            }
        }
        for i in self.players.len()..players.len() {
            self.players.push(PlayerDraw::new(
                ctx,
                players[i].clone(),
                &self.font,
                i,
                self.screen_size.1,
            ));
        }

        let board: Board = serde_json::from_value(r["board"].clone()).unwrap();
        for i in 0..board.numbers.len() {
            if board.numbers[i].found {
                self.board[i].found = true;
                self.board[i].color = board.numbers[i].color;
            }
        }

        if self.last_number.contents() != board.last_number.to_string().as_str() {
            self.last_number =
                Text::new(ctx, board.last_number.to_string().as_str(), &self.font).unwrap();
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);

        graphics::set_color(ctx, graphics::WHITE)?;
        graphics::draw(
            ctx,
            &self.last_number,
            Point2::new((self.screen_size.1 + 10) as f32, 0.0),
            0.0,
        )?;

        for n in &mut self.board {
            n.draw(ctx)?;
        }

        for p in &mut self.players {
            p.draw(ctx)?;
        }

        graphics::present(ctx);
        Ok(())
    }

    fn mouse_button_down_event(&mut self, _ctx: &mut Context, button: MouseButton, x: i32, y: i32) {
        if button == MouseButton::Left {
            let x = x as u32;
            let y = y as u32;
            if x > 0 && x < self.screen_size.1 && y > 0 && y < self.screen_size.1 {
                let h = self.screen_size.1 / self.board_size;
                let i = y / h * 10 + x / h;
                let msg = format!(
                    "{{\"action\": \"click\", \"i\": {},\"id\": {}}}",
                    i, self.id
                );
                let _ = self.request(msg);
            }
        }
    }
}
