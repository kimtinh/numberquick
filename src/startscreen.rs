/*
 * Copyright (C) 2018 Do Thanh Trung <dothanhtrung.16@gmail.com>
 */

use ggez::event::{self, Keycode, Mod, MouseButton};
use ggez::graphics::{self, DrawMode, Font, Point2, Text};
use ggez::{Context, GameResult};

pub struct TextBox {
    pub text: Text,
    pos: (f32, f32),
    w: f32,
    h: f32,
    margin: f32,
    bold: f32,
    font: Font,
}

impl TextBox {
    fn new(_ctx: &mut Context, text: &str, font: Font, pos: (f32, f32)) -> Self {
        let margin = 5.0;
        let h = font.get_height() as f32 + 2.0 * margin;
        let w = font.get_width(text) as f32 + 2.0 * margin;
        TextBox {
            text: Text::new(_ctx, text, &font).unwrap(),
            pos,
            w,
            h,
            margin,
            bold: 2.0,
            font,
        }
    }

    fn append(&mut self, _ctx: &mut Context, text: String) {
        let text = self.text.contents().to_owned() + &text;
        self.text = Text::new(_ctx, &text, &self.font).unwrap();
    }

    fn backspace(&mut self, _ctx: &mut Context) {
        let mut s = self.text.contents().to_string();
        s.pop();
        self.text = Text::new(_ctx, &s, &self.font).unwrap();
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::rectangle(
            ctx,
            DrawMode::Line(self.bold),
            [self.pos.0, self.pos.1, self.w, self.h].into(),
        )?;
        graphics::draw(
            ctx,
            &self.text,
            Point2::new(self.pos.0 + self.margin, self.pos.1 + self.margin),
            0.0,
        )?;

        Ok(())
    }

    fn is_clicked(&mut self, clicked_pos: (f32, f32)) -> bool {
        if clicked_pos.0 > self.pos.0
            && clicked_pos.0 < self.pos.0 + self.w
            && clicked_pos.1 > self.pos.1
            && clicked_pos.1 < self.pos.1 + self.h
        {
            true
        } else {
            false
        }
    }
}

enum Focus {
    Name,
    Server,
}
pub struct StartState {
    pub is_host: bool,
    pub name: TextBox,
    pub server: TextBox,
    host_btn: TextBox,
    connect_btn: TextBox,
    focus: Focus,
}

impl StartState {
    pub fn new(_ctx: &mut Context, screen_size: (u32, u32)) -> GameResult<StartState> {
        let font = Font::default_font().unwrap();
        let ex_margin = 40.0;
        let in_margin = 10.0;

        let name_pos = (screen_size.0 as f32 / 6.0, screen_size.1 as f32 / 6.0);
        let mut name = TextBox::new(_ctx, "Player name", font.clone(), name_pos);
        name.w = screen_size.0 as f32 / 3.0 * 2.0;

        let host_btn_y = name_pos.1 + name.h + ex_margin;
        let mut host_btn = TextBox::new(_ctx, "HOST GAME", font.clone(), (0.0, host_btn_y));
        host_btn.pos.0 = (screen_size.0 as f32 - host_btn.w) / 2.0;

        let connect_btn_y = host_btn_y + host_btn.h + ex_margin;
        let mut connect_btn = TextBox::new(_ctx, "CONNECT", font.clone(), (0.0, connect_btn_y));
        connect_btn.pos.0 = (screen_size.0 as f32 - connect_btn.w) / 2.0;

        let server_pos = (
            screen_size.0 as f32 / 6.0,
            connect_btn_y + connect_btn.h + in_margin,
        );
        let mut server = TextBox::new(_ctx, "127.0.0.1", font.clone(), server_pos);
        server.w = screen_size.0 as f32 / 3.0 * 2.0;

        let s = StartState {
            is_host: false,
            name,
            server,
            host_btn,
            connect_btn,
            focus: Focus::Name,
        };
        Ok(s)
    }

    fn reset_bold(&mut self) {
        self.name.bold = 2.0;
        self.server.bold = 2.0;
        self.host_btn.bold = 2.0;
        self.connect_btn.bold = 2.0;
    }
}

impl event::EventHandler for StartState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);

        self.name.draw(ctx)?;
        self.host_btn.draw(ctx)?;
        self.connect_btn.draw(ctx)?;
        self.server.draw(ctx)?;

        graphics::present(ctx);
        Ok(())
    }

    fn mouse_button_down_event(&mut self, ctx: &mut Context, button: MouseButton, x: i32, y: i32) {
        if button == MouseButton::Left {
            let x = x as f32;
            let y = y as f32;
            let clicked_pos = (x, y);
            if self.name.is_clicked(clicked_pos) {
                self.focus = Focus::Name;
                self.reset_bold();
                self.name.bold *= 2.0;
            } else if self.server.is_clicked(clicked_pos) {
                self.focus = Focus::Server;
                self.reset_bold();
                self.server.bold *= 2.0;
            } else if self.host_btn.is_clicked(clicked_pos) {
                self.is_host = true;
                let _ = ctx.quit();
            } else if self.connect_btn.is_clicked(clicked_pos) {
                let _ = ctx.quit();
            }
        }
    }

    fn key_down_event(&mut self, ctx: &mut Context, keycode: Keycode, _keymod: Mod, _repeat: bool) {
        match keycode {
            Keycode::Backspace => match self.focus {
                Focus::Name => {
                    self.name.backspace(ctx);
                }
                Focus::Server => {
                    self.server.backspace(ctx);
                }
            },
            _ => (),
        }
    }

    fn text_input_event(&mut self, ctx: &mut Context, text: String) {
        match self.focus {
            Focus::Name => {
                self.name.append(ctx, text);
            }
            Focus::Server => {
                self.server.append(ctx, text);
            }
        }
    }
}
