/*
 * Copyright (C) 2018 Do Thanh Trung <dothanhtrung.16@gmail.com>
 */

extern crate rand;
extern crate serde_derive;

use self::rand::Rng;
use self::serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct Player {
    id: u64,
    pub name: String,
    pub score: u32,
    pub color: (f32, f32, f32),
}

impl Player {
    pub fn new(id: u64, name: String) -> Self {
        Player {
            id,
            name,
            score: 0,
            color: (
                rand::thread_rng().gen_range(0.0, 1.0),
                rand::thread_rng().gen_range(0.0, 1.0),
                rand::thread_rng().gen_range(0.0, 1.0),
            ),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Number {
    pub number: u32,
    pub found: bool,
    pub color: (f32, f32, f32),
}

#[derive(Serialize, Deserialize)]
pub struct Board {
    pub size: u32,
    pub numbers: Vec<Number>,
    pub last_number: u32,
}

impl Board {
    pub fn new(size: u32) -> Self {
        let number_size = size * size;
        let mut all_numbers = Vec::new();
        for i in 0..number_size {
            all_numbers.push(i + 1);
        }

        let mut numbers = Vec::new();
        let mut number_remain = number_size;
        for _ in 0..number_size {
            let num = rand::thread_rng().gen_range(0, number_remain) as usize;
            numbers.push(Number {
                number: all_numbers[num],
                found: false,
                color: (1.0, 1.0, 1.0),
            });
            all_numbers.remove(num);
            number_remain -= 1;
        }

        Board {
            size,
            numbers,
            last_number: 0,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct GameData {
    pub players: Vec<Player>,
    pub board: Board,
}

impl GameData {
    pub fn new(size: u32) -> Self {
        GameData {
            players: Vec::new(),
            board: Board::new(size),
        }
    }

    pub fn check(&mut self, i: usize, id: u64) {
        if self.board.last_number + 1 == self.board.numbers[i].number {
            self.board.numbers[i].found = true;
            for p in &mut self.players {
                if p.id == id {
                    self.board.numbers[i].color = p.color;
                    p.score += 1;
                    self.board.last_number += 1;
                }
            }
        }
    }
}
