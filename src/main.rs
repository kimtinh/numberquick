/*
 * Copyright (C) 2018 Do Thanh Trung <dothanhtrung.16@gmail.com>
 */

mod gamedata;
mod mainscreen;
mod server;
mod startscreen;

use mainscreen::MainState;
use server::Server;
use startscreen::StartState;

extern crate ggez;
extern crate serde_json;
extern crate tokio;

use self::ggez::event;
use std::thread;
use tokio::prelude::*;

fn main() {
    let screen_size = (800, 640);
    let ctx = &mut ggez::ContextBuilder::new("numberquick", "kim tinh")
        .window_setup(ggez::conf::WindowSetup::default().title("Number Quick!"))
        .window_mode(ggez::conf::WindowMode::default().dimensions(screen_size.0, screen_size.1))
        .build()
        .expect("Failed to build ggez context");

    let s = &mut StartState::new(ctx, screen_size).unwrap();

    event::run(ctx, s).unwrap();
    let mut server_addr = s.server.text.contents().to_string();
    server_addr.push_str(":11211");
    let mut server_addr = server_addr.as_str();
    
    let localhost = "127.0.0.1:11211";

    if s.is_host {
        server_addr = localhost;
        thread::spawn(move || {
            let server = Server::new(10, localhost);
            println!("Listening on: {}", server.socket.local_addr().unwrap());
            tokio::run(server.map_err(|e| println!("server error = {:?}", e)));
        });
    }

    let main = &mut MainState::new(
        ctx,
        server_addr,
        s.name.text.contents().to_string(),
        screen_size,
    ).unwrap();

    event::run(ctx, main).unwrap();
}
